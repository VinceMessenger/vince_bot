//****************************************************************************
// Vince Messenger
// vince_bot
// A classy slack bot to handle gif requests and other assistant like tasks
//****************************************************************************

var Botkit = require('botkit');
var http = require('http');
var config = require('./config/config');
var request = require('request');
var translate = require('google-translate-api');

var translate_req_langs = require('./config/supported_languages').supported_langs;

var gen_vince_resp = config.generic_vince_reponses;

var gif_message = {
	'type': 'message',
	'text': 'Enjoy this random gif I found',
  'attachments': [
      {
          'fallback': 'Hourly Gif Update',
          'title': 'Gif Update',
      }
  ],
};

var num_gifs = 100;
var random_index;
var gif_path = '/v1/gifs/search?api_key=dc6zaTOxFJmzC&q=';   // this is a beta api_key, no need to hide details until prod key

// create giphy request options
var giphy_req = {
  host: 'api.giphy.com',
  path: ''  // will be filled in later
};

var slack_token = config.slack_token;

var controller = Botkit.slackbot({});
var bot = controller.spawn({
  token: slack_token
});

// use RTM
bot.startRTM(function(err,bot,payload) {
  if(err)
  	throw err;
  	
  console.log('Connected to Slack!');
});

// vince_bot gif search functionality
controller.hears('gif', ['direct_mention', 'direct_message'], function(bot,message) {

  // split string to find gif search criteria
  var search_criteria = message.text.substring(message.text.indexOf("gif") + 4, message.length);

  // add search elements to array
  var search_array = search_criteria.split(" ");

  // now add search criteria to giphy api gif search url
  var search_path = gif_path;
  for (var i = 0; i < search_array.length; i++) {
    search_path += search_array[i];

    if (i < search_array.length-1){
      search_path += '+';
    }
  }

  getGif(search_path, function(url){

    // check if we got results from search
    if (!url){
      bot.reply(message, "Sorry homie, I didn't find any gifs for " + search_criteria + ".");
    }
    else{
      // set message properties
      gif_message['attachments'][0].image_url = url;
      gif_message['attachments'][0].title = search_criteria;

      // send response
      bot.reply(message, gif_message, function(err, res) {
        if(err)
          throw err;
      });
    }

  });
});

// giphy request function
var getGif = function(search_path, cb) {

  // first add newly generated search_path to req object
  giphy_req.path = search_path;

	http.get(giphy_req, function(res){
		var body = "";
		res.on('data', function(d) {
    	body += d;
    });
    
    res.on('end', function() {
    	var parsed = JSON.parse(body);
      var return_val;
      
      // set random_index for gif array
      random_index = Math.floor(Math.random() * parsed.data.length);

      // clear out giphy_req.path
      giphy_req.path = '';

      if(parsed.data.length == 0){
        return_val = false;
      }
      else{
        return_val = parsed.data[random_index].images.original.url;
      }
			cb(return_val);
    });
	});
};

controller.hears('bot', ['direct_mention'], function(bot, message){
  bot.reply(message, '#notabot');
});

///////// language translate functionality /////////
controller.hears('translate', ['direct_mention', 'direct_message'], function(bot,message) {

  // get dest language
  var lang_substr = message.text.substring(message.text.indexOf("-"));   // get substring starting with -
  var lang = lang_substr.substring(1, lang_substr.indexOf(" "));
  var lang_cd = translate_req_langs[lang.toLowerCase()];
  if (!lang_cd){
    console.log(lang);
    bot.reply(message, "That language is not supported yet.");
    return;
  }

  // get text to translate
  var translate_text = message.text.substring(message.text.indexOf("-") + lang.length + 2);  // add 2 - one to get to space, one to get to first char of translate_text

  // setup translate request options
  var translate_options = {};
  translate_options.translate_text = translate_text;
  translate_options.lang_cd = lang_cd;

  // call translate request function
  getTranslation(translate_options, function(translated_text){
    // send response
    bot.reply(message, translated_text);
  });
});

// translate request function
var getTranslation = function(translate_options, cb){

  translate(translate_options.translate_text, {to: translate_options.lang_cd}).then(res => {
    var return_val = res.text;
    cb(return_val)
  }).catch(err => {
    console.error(err);
  });
};
///////// end language translate functionality //////////

// return generic Vince response if 
controller.hears('', ['direct_mention'], function(bot,message) {
  // get random index for generic vince_response array
  random_index = Math.floor(Math.random() * gen_vince_resp.length);

  // reply to _message_ by using the _bot_ object
  bot.reply(message, gen_vince_resp[random_index]);
});